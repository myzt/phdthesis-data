#paperfull01 runs 1, 2, 3 
cp paperlatest1/pts.txt pts-01.txt
cp paperlatest1/csv.txt csv-01.txt

#paperlatest2 runs 4, 5, 6
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+10;print}' paperlatest2/pts.txt > pts-02.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+10;print}' paperlatest2/csv.txt > csv-02.txt

#paperlatest3 runs 7, 8, 9, 10
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+20;print}' paperlatest3/pts.txt > pts-03.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+20;print}' paperlatest3/csv.txt > csv-03.txt

cat pts-01.txt pts-02.txt pts-03.txt > pts.txt
cat csv-01.txt csv-02.txt csv-03.txt > csv.txt
