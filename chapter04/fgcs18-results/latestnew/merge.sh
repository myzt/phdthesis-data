#paperfull01 runs 1, 2, 3 
cp latestnew1/pts.txt pts-01.txt
cp latestnew1/csv.txt csv-01.txt

#paperlatest2 runs 4, 5, 6
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+10;print}' latestnew2/pts.txt > pts-02.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+10;print}' latestnew2/csv.txt > csv-02.txt

#paperlatest3 runs 7, 8, 9, 10
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+20;print}' latestnew3/pts.txt > pts-03.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+20;print}' latestnew3/csv.txt > csv-03.txt

#paperlatest4 
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+30;print}' latestnew4/pts.txt > pts-04.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+30;print}' latestnew4/csv.txt > csv-04.txt

#paperlatest5 
awk 'BEGIN {FS=","} {OFS=","} {$4=$4+40;print}' latestnew5/pts.txt > pts-05.txt
awk 'BEGIN {FS=","} {OFS=","} {$3=$3+40;print}' latestnew5/csv.txt > csv-05.txt

cat pts-01.txt pts-02.txt pts-03.txt pts-04.txt pts-05.txt > pts.txt
cat csv-01.txt csv-02.txt csv-03.txt csv-04.txt csv-05.txt > csv.txt
