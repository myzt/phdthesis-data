import pandas as pd
from .integrate import integrate

def read_csv(filename):
    '''Reads sarnet CSV data'''
    df = pd.read_csv(filename, header=None)
    df = df.reset_index()
    row_length = len(list(df[:0])) 
    if row_length == 10:
        df.columns = ["Index", "AttDef", "Attackers", "Run", "Detect", "Implement1", "Implement2", "Recover", "Recover_init", "Mean"]
    df['d_detect_i1'] = df['Implement1'] - df['Detect']
    df['d_i1_i2'] = df['Implement2'] - df['Implement1']
    if 'udp' in filename: 
        df['d_i2_recover'] = df['Recover'] - df['Implement2']
    else:
        df['d_i2_recover'] = df['Recover_init'] - df['Implement2']
        df['d_recinit_recover'] = df['Recover'] - df['Recover_init']
    return df 

def read_pts(filename):
    '''Reads sarnet PTS data'''
    df = pd.read_csv(filename, header=None)
    df.columns=['AttDef','Type','Attack','Run','Mean','Values', 'Times']
    return df


def read_results(basedir, thresholds):
    '''Reads results produced by the SARNET-agent in basedir and returns a dataframe'''

    def sanitize(data_full, timeout=None):
        '''Converts complex datatypes to pythonic version and adds extra values'''
        for index, entry in data_full.iterrows():
            #convert values to list of floats
            values = [float(y) for y in entry['Values'].split(',')]
            data_full.at[index, 'Values'] = values
            #convert times to list of floats
            times = entry['Times'].split(',')
            times =  [float(t) - float(times[0]) for t in times]
            data_full.at[index, 'Times'] = times
            #calculate and add integral between detect and recover_init
            #.*_r15 indicates a relax of 15 
            relax = entry['AttDef'].split('_')[-1][1:]
            data_full.at[index, 'Relax'] = relax
        return data_full

    def calculate_impacts(data_full, negate_metrics = [],  debug = False):
        for index, entry in data_full.iterrows():
            negate = False

            if entry['Type'] in negate_metrics:
                negate = True

            integral = integrate(thresholds[entry['Type']], 
                                              entry['Times'],  
                                              entry['Values'], 
                                              float(entry['Detect']), 
                                              float(entry['Recover_init']),
                                              negate=negate, normalize=False, debug=debug)
            
            data_full.at[index, 'Integral'] = integral
        return data_full


    csv = read_csv(basedir + '/csv.txt')
    pts = read_pts(basedir + '/pts.txt')
    data_full = pts.merge(csv, on = ['AttDef', 'Run'], how = 'left')
    data_full = sanitize(data_full)
    data_full = calculate_impacts(data_full, negate_metrics=['sales'])
    data_full.set_index(['AttDef', 'Run', 'Type'], inplace=True)
    data_full.sort_index(inplace=True) 
    return data_full

def summarize(data_full, filter_failed=False, filter_nd=True, timeout=None):
    def parse_attdef(df):
        for index, entry in df.iterrows():
            attack, size, window, defence, relax = entry['AttDef'].split('_')
            window = window[1:]
            relax = relax[1:]
            defence = defence[1:]
            df.at[index, 'Attack'] = attack
            df.at[index, 'Size'] = size
            df.at[index, 'Window'] = window
            df.at[index, 'Defence'] = defence
            df.at[index, 'Relax'] = relax
        return df


    def failures(col):
        print(col)
        return col

    import numbers

    #timeouts to NaN (this is a good thing)
    data_full = data_full.applymap(lambda x: x if not isinstance(x, numbers.Number) or x > -10 else None)

    #filter recovery before detect
    data_full.loc[data_full['Recover_init'] < data_full['Detect'], 'Recover_init'] = None
    data_full.loc[data_full['Recover_init'] < data_full['Detect'], 'Recover'] = None

    gr = data_full.groupby(['AttDef', 'Type'])
    #filter here in stead of before for group stats

    notdetect = gr.agg({'Detect': lambda r: r.size - r.count()})
    detected = gr.agg({'Detect': lambda r: (r.count()/r.size) *100})
    total = gr.agg({'Detect': lambda r: r.size})
    #print(notdetect)

    if filter_nd:
        data_full = data_full[data_full['Detect'].notnull()]

    if filter_failed:
        data_full = data_full[data_full['Recover'].notnull()]

    if timeout: 
        data_full['Recover'] = data_full['Recover'].apply(lambda x: x if not isinstance(x, numbers.Number) or x < timeout else None)
        data_full['Recover_init'] = data_full['Recover_init'].apply(lambda x: x if not isinstance(x, numbers.Number) or x < timeout else None)


    gr_attdef_type = data_full.groupby(['AttDef', 'Type'])

    means = gr_attdef_type.mean()
    error = gr_attdef_type.sem()
    success = gr_attdef_type.agg({'Recover':lambda r: (r.count()/r.size)*100})
    count = gr_attdef_type.agg({'Recover':lambda r: r.count()})
    size = gr_attdef_type.agg({'Recover':lambda r: r.size})

    data_sum = pd.concat([means, error, success, notdetect, total, detected, size], axis=1)
    data_sum.columns = [name for name in means.columns] + ['e_' + name for name in error.columns] + ['success' , 'notdetect' , 'total', 'detected', 'size' ]
    data_sum = data_sum.dropna(axis=1, how='all').reset_index()
    #.*_r15 indicates a relax of 15 
    data_sum = parse_attdef(data_sum)
    data_sum.set_index(['Attack','Size','Defence', 'Relax', 'Type'], inplace=True)
    data_sum.sort_index(inplace=True) 
    return data_sum


def lookup_row(data, query):
    '''looks up single (unique) row in dataset based on query'''
    assert isinstance(query, dict) 
    for k, v in query.items(): 
        data = data[data[k] == v]
    data = data.reset_index()
    #assert len(data) == 1, 'lookup did not return unique row: rows = {}'.format(len(data))
    return data

def timeout(data, label, run=None, observable=None):
    #fr = lookup_row(data, {'AttDef': label, 'Run':run})
    if data['Recover_init'][0] < 0: 
        return True
    return False

def classify_size(df):
    """Normalizes attack size by categorizing them using Light Medium and Heavy"""
    df = df.reset_index()
    for idx, data in df.iterrows():
        if data['Size'] in ['1', '15M']:
            size = 0 
        elif data['Size'] in ['5', '20M']:
            size = 'Light'
        elif data['Size'] in ['10', '30M']:
            size = 'Medium'
        elif data['Size'] in ['15', '40M']:
            size = 'Heavy'
        df.at[idx, 'Size'] = size 
    return df 
    

