from scipy.integrate import trapz 
from scipy.interpolate import interp1d
import numpy as np

def integrate(threshold, xs, ys, start, end, negate=False, normalize=False, debug=False):
    if end > start:
        # get the datapoints between Detect and Recover_init
        try: 
            xs, ys = zip(*[ (x,y) for x, y in zip(xs, ys) if x >= start and x <= end ])
        except ValueError: 
            #raise ValueError('no values between start and end')
            #print('warn: no values between start and end')
            return 0
        # set an artificial maximum of 2 times the threshold
        ceiling = threshold * 2

        ys = [y if y < ceiling else ceiling for y in ys] 
        # set the threshold as 0
        ys = [y - threshold for y in ys]
        # get the integral
        if negate:
            ys = [-1 * y for y in ys] #sales are negative so we invert among axis
        ys = [y if y > 0 else 0 for y in ys] # we dont care about anything negative
        ig = trapz(ys,xs)
           
        #yz = [y for y in ys if y <= 0]           # data.loc([data['AttDef'] == str(label)])['Integral'] = ig
        if ig == 0  and debug:
            print('integrate xs,', xs) 
            print('integrate ys', ys)
        if normalize:
            return ig/(threshold * (end - start))
            return ig/(threshold * (end - start))
            #return ig/threshold
        else:
            return ig


    
def calc_avg_measurements(values, datapoints=40):
    #xs, ys = values

    #print (ys)

    xnew = np.linspace(0, datapoints, num=datapoints, endpoint=True)
    #print([(xs, ys) for xs, ys, in values])
    normalized = [ interp1d(xs, ys, bounds_error=False )(xnew) for xs, ys, in values] 
    result = [sum(v)/len(values) for v in zip(*list(normalized))]
    return result
