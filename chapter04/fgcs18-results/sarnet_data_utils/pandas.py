import numbers

def color_negative_red(val):
    """
    Takes a scalar and returns a string with
    the css property `'color: red'` for negative
    strings, black otherwise.
    """
    color = 'red' if isinstance(val, numbers.Number) and val < 0 else 'black'
    return 'color: %s' % color
