from matplotlib import pyplot
import matplotlib.pyplot as plt
import seaborn as sns

def plot_dp(data, colname, color='black', style='solid', run=None, label='', timeout=40):
    if run:
        value = data[colname][0]
    else: 
        value = data[colname][0]
    if value < -timeout: #timeout
        plt.axvline(0, color='red')
        plt.axvline(1, color='red')
    return plt.axvline((value), color=color, linestyle=style, label=label)  

def get_color(label): 
    palette = sns.color_palette("colorblind")
    color_map = { 'sales' : palette[0], 'logfail': palette[1], 'cpu': palette[3]}
    if label in color_map.keys():
        return color_map[label]
    else:
        return None

def get_marker(label):
    marker_map = { 'sales' : 'o', 'logfail': '^', 'cpu': 'v'}
    if label in marker_map.keys():
        return marker_map[label]
    else:
        return '.'


def plot_attdef(label, data, run=None, thresholds={}, timeout=40):
    if run:
        data = data.loc[label, run]
    else:
        data = data.loc[label]
        
    r = run
    
    plt.figure(figsize=(20,10))    
    
    lines = []
    
    #draw thresholds
    for name, threshold in thresholds.items():
        lines.append(plt.plot([threshold]*(timeout+1), label=name+'_thresh', linestyle='--', color=get_color(name)))
    
    #draw datapoints
    for idx, row in data.iterrows():
        if idx in thresholds.keys():
            lines.append(plt.plot(row['Times'], row['Values'], "-o", label=idx, color=get_color(idx), marker=get_marker(idx), markersize=10)) # plot individual
        
    #draw detection points
    t1 = plot_dp(data, 'Detect', color='red', run=r, label='Detect')   
    t2 = plot_dp(data, 'Implement1', color='blue', style='dashed', run=r, label='Implement1')
    t3 = plot_dp(data, 'Implement2', color='cyan', style='dotted', run=r, label='Implement2')
    t4 = plot_dp(data, 'Recover_init', color='green', run=r, label='Recover init')
    t5 = plot_dp(data, 'Recover', color='darkgreen', run=r, label='Recover')
    det_points = [t1, t2, t3, t4, t5]
    
    #cosmetics
    plt.xlim(0,timeout)
    plt.ylabel('Amount')
    plt.xlabel('Time in seconds')
    plt.grid(b=True, which='both', color='0.65',linestyle='-')    
    det_legend = plt.legend(det_points, ['Detect', 'Implement1', 'Implement2', 'Recover_init', 'Recover'], loc=1, title='Vertical lines')
    det_legend.set_title("Vertical", prop = {'size':'xx-large', 'weight':'bold'})
    leg = plt.legend(handles=[l[0] for l in lines], loc=4, title='Horizontal')
    leg.set_title("Horizontal", prop = {'size':'xx-large', 'weight':'bold'})
    plt.gca().add_artist(det_legend)
    return plt
