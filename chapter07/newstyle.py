#!/usr/bin/env python
# coding: utf-8

# In[1]:


# get_ipython().run_line_magic('matplotlib', 'inline')
import csv
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import statistics
import warnings
import numpy as np
from pprint import pprint

from collections import namedtuple
from integrate import calc_efficiency2 as calc_efficiency

from scipy import stats
import ast
from collections import OrderedDict


def read_records(filename):
    data = open(filename, "r").read()
    records = list(csv.DictReader(data.splitlines()))
    return records


def read_params(filename):
    params = open("{}.params".format(filename), "r").read()
    all_params = list(csv.DictReader(params.splitlines()))
    return all_params


def parse_id(id):
    attack = id.split("(")[0]
    params = ast.literal_eval(id.split("(")[1][:-1])
    attackers, victims, depth, size = params
    return {
        "attack": str(attack),
        "attackers": list(attackers),
        "victim": eh(victims),
        "depth": int(depth),
        "size": float(size),
    }


def efficiency_cscloud(p, r, alpha_override=None, beta_override=None, recover_override=None):
    """something went wrong so we have to recalculate efficiency is this fixed in agent code?"""
    attack = p["attack"]
    weights = ast.literal_eval(r["settings"])["weights"]
    impacts = ast.literal_eval(r["impacts"])[eh("service101")]
    for w in weights[attack].keys():
        if w not in impacts.keys():
            impacts[w] = np.nan

    thresh = ast.literal_eval(r["thresholds"])[eh("service101")]
    negates = {k: False if i == "sales" else True for k, i in impacts.items()}
    timeout = int(p["timeout"])
    total_cost = int(r["total_cost"])
    budget = int(p["budget"])
    recovered = bool(int(r['recover']))
    alpha = ast.literal_eval(r["settings"])["alpha"]
    if alpha_override:
        alpha = alpha_override
    beta = ast.literal_eval(r["settings"])["beta"]
    if beta_override:
        beta = beta_override

    if recover_override is not None:
        recovered = recover_override

    efficiency = calc_efficiency(
        attack,
        impacts,
        weights[attack],
        thresh,
        negates,
        timeout,
        total_cost,
        budget,
        recovered,
        alpha,
        beta,
    )
    return efficiency


def is_host(host):
    """checks whether host is one of the known types"""
    types = ['service', 'transit', 'nfv', 'client']
    return any([True for t in types if host.startswith(t)])

def eh(host):
    """expands hostname to fqdn"""
    assert is_host(host), 'unknown host type'
    if host.endswith('sarnet-sc17-dev'):
        return host #noop
    domain = 'sarnet-sc17-dev'
    if host == 'service101':
        return "service101.as101.{}".format(domain)
    else:
        domain_id = host[-2:]
        return "{}.as{}.sarnet.{}".format(host, domain_id, domain)

def sh(host):
    """shortens hostname from fqdn"""
    assert is_host(host), 'unknown host type'
    return host.split('.')[0]

def to_float(ip):
    try:
        return float(ip)
    except:
        return np.nan

def get_efficiency(p, r, victim="service101"):
    """Returns the proper (cscloud) efficiency
       in case of old datasets, the oldstyle efficiency is verfied
       a new cscloud style calculation is returned.
       if the oldstyle efficiency does not match throuw an error.
    """
    efficiency = to_float(ast.literal_eval(r["efficiency"])[eh(victim)])
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        good_efficiency = efficiency_cscloud(p, r)
        good_efficiency_norecover = efficiency_cscloud(p, r, recover_override=True)
        oldstyle_efficiency = efficiency_cscloud(p, r, alpha_override=1)
        if efficiency == good_efficiency:
            return (True, efficiency, good_efficiency)
        elif efficiency == good_efficiency_norecover:
            print('warn: efficiency says recovery but marked as not recovered', p['source'])
            print('warn: run_efficiency', efficiency, 'recovered_efficiency', good_efficiency, 'recovered_false_efficiency', good_efficiency_norecover)
            return (True, efficiency, good_efficiency_norecover)
        else:
            if efficiency == oldstyle_efficiency:
                return (True, good_efficiency, oldstyle_efficiency)
            else:
                #return (True, good_efficiency, oldstyle_efficiency)
                raise ValueError("efficiency no match {} {} {}, efficiency".format( good_efficiency, efficiency, oldstyle_efficiency))
                return (False, efficiency, oldstyle_efficiency)

def prep_df(records, all_params=None, victim="service101.as101.sarnet-sc17-dev"):
    effl = []
    run = 0
    p = None
    for r in records:
        p = [param for param in all_params.copy() if int(param["oid"]) == int(r["oid"])]
        if len(p) > 1:
            for par in p:
                assert p[0] == par, "multi-parameter unequal"

        p = p[0]
        assert p["oid"] == r["oid"], "record id does not match parameter id"
        assert "seq" in r.keys(), "no seq field, is this an old dataset?"
        match, efficiency, efficiency2 = get_efficiency(p, r, victim)
        assert match == True, 'the recalculated efficiency does not match the value in record'

        params = {
            "attack": p["attack"],
            "approach": int(r["approach"]),
            "size": p["size"],
            "level": p["level"],
            "efficiency": to_float(efficiency),
            "efficiency2": to_float(efficiency2),
            'impact': to_float(ast.literal_eval(r["impacts"])[eh(victim)].get("sales")),
            "recover": r["recover"],
            "detect": r["detect"],
            "cost": int(ast.literal_eval(r["total_cost"])),
            "waittime": 3,
            #'waittime': int(p['ap3_recovery_waittime']),
            "fixed": int(p["fixed"]),
            "budget": int(p["budget"]),
            "attackers": ",".join(
                [a.split(".")[0] for a in ast.literal_eval(p["attackers"])]
            ),
            "oid": ",".join([r["oid"], p["oid"]]),
            "pid": int(p["oid"]),
            "rid": int(r["oid"]),
            "seq": int(r["seq"]),
            "run": run,
            "evidence": ast.literal_eval(r["evidence"]),
            "start": r['init'],
            "file": r['source'],
            "ranking": ast.literal_eval(r['solverstate'])
        }
        effl.append(params)

    columns = effl[0].keys()
    data = list(effl)
    df = pd.DataFrame(data, columns=columns)
    df["efficiency"] = df["efficiency"].replace(-1, 0)
    return df


def megaplot2(df, axis, plot_sep=None):
    sns.set(rc={"figure.figsize": (11.7, 8.27)})
    ax = sns.scatterplot(x=axis[0], y=axis[1], style=plot_sep, data=df, palette="muted")
    ax.legend(loc=5)


def megaplot3(df, axis, plot_sep=None):
    sns.set(rc={"figure.figsize": (11.7, 8.27)})
    df_all = df
    df_grouped = df_all.groupby(["situation", plot_sep, axis[0]])
    df = df_grouped.mean()
    if len(df_grouped.efficiency) > 1:
        df["STDEV"] = df_grouped.efficiency.apply(statistics.stdev)
        df["SEM"] = df_grouped.efficiency.apply(stats.sem)

        # stats.t.ppf(1-0.025, 2) #len-1
        df["CI"] = df_grouped.efficiency.apply(stats.sem) * 1.96
        # print((df_grouped.efficiency))
        df["CI"] = df_grouped.efficiency.apply(stats.sem) * stats.t.ppf(
            1 - 0.025, 2
        )  # len-1
    else:
        df["STDEV"] = 0
        df["SEM"] = 0
        df["CI"] = 0
    df.reset_index(inplace=True)
    # df['marker'] = df['approach'].map(markers)
    xticks = sorted(list(set(df[axis[0]])))
    print(xticks)

    g = sns.FacetGrid(
        df,
        col="situation",
        hue="approach",
        palette="muted",
        col_wrap=2,
        hue_kws=dict(marker=["o", "^", "v"]),
    )
    g.map(plt.errorbar, axis[0], axis[1], "STDEV", fmt=".").set_titles(
        "{col_name}"
    ).set(xticks=xticks)
    for ax in g.axes.flat:
        ax.set_title(fullsit(ax.get_title()))
        if ax.get_xlabel() == "size":
            ax.set_xlabel("attack size")
            ax.set_xticklabels(xticks, rotation=40, ha="center")
            ax.set_xticklabels(xticks, fontsize=8)
        elif ax.get_xlabel() == "level":
            ax.set_xlabel("alliance size")

    g.add_legend(title="Algorithm")



def read_all(f_list, params=False):
    """Reads record or parameter sets from file list"""
    """combines them into one OrderedDict and adds a unique sequence number to each record"""
    def reorder(r, order):
        newdict = OrderedDict([(k, None) for k in order if k in r])
        newdict.update(r)
        return newdict

    record_sep = 1000
    all_records = []
    orderr = None
    offset = 0
    for n, filename in enumerate(f_list):
        if params:
            records = read_params(filename)
        else:
            records = read_records(filename)
        assert len(records) < record_sep
        counter = offset
        for counter, r in enumerate(records):
            if not orderr:
                orderr = list(r.keys())
            else:
                r = reorder(r, orderr)
            r["source"] = filename
            #r["oid"] = str(n * record_sep + int(r["id"]))
            r["oid"] = r["id"]
            r["id"] = str(counter + offset)
            all_records.append(r)
        offset = offset + len(records)
    return all_records



Plot = namedtuple("Plot", ["name", "x", "y", "sep"])
attacks = ["eq2attackerfarnear", "fixedcosts", "increaselevel", "moveattacker"]

scenarios = [
    Plot("budget", "budget", "efficiency", "approach"),
    Plot("load", "size", "efficiency", "approach"),
    Plot("size", "level", "efficiency", "approach"),
]

situations = ["1n", "1f", "2s", "al"]


def fullsit(situation):
    fullsit = {
        "1n": "single attacker close",
        "1f": "single attacker far",
        "2s": "two attackers 1 foo far 1 close",
        "al": "all clients attacking",
    }
    return fullsit[situation]


def build_df(func):
    def wrapper(scenario, situation):
        f_list = func(scenario, situation)
        records = read_all(f_list)
        params = read_all(f_list, params=True)
        df = prep_df(records, params)
        df = df.replace({'efficiency': {0: np.NaN}})
        df = df.replace({'efficiency2': {0: np.NaN}})
        return df

    return wrapper


def build_mdf(func, modify_func=None, situations = None, scenario=None):
    dfs = [func(scenario, situation) for situation in situations]
    mdf = pd.concat(dfs, keys=situations, names=["situation"])
    mdf = mdf.reset_index(level=0)
    if modify_func:
        mdf = modify_func(mdf)
    return mdf


# # Efficiency vs Budget

# In[3]:


def multi_run_plot(mdf, axis, sep):
    sns.set(rc={"figure.figsize": (11.7, 8.27)})

    df_grouped = mdf.groupby(["situation", "rid"])
    df = df_grouped.mean()
    gmdf = mdf.groupby(["situation", "rid"])["efficiency"].agg(["mean"])
    gmdf = pd.DataFrame(gmdf)  # .reset_index()
    df["STDEV"] = df_grouped.efficiency.apply(statistics.stdev)
    gmdf = df
    cmdf = pd.DataFrame(df.groupby(["situation"]).cumcount())
    cmdf = cmdf.rename(columns={0: "idx"})
    gmdf = gmdf.join(cmdf)
    mdf = gmdf.reset_index()

    g = sns.FacetGrid(mdf, col=sep, col_wrap=2)
    xticks = [x for x in mdf[axis[0]]]
    g.map(plt.errorbar, axis[0], axis[1], "STDEV", fmt=".").set_titles(
        "{col_name}"
    ).set(xticks=xticks)

    for ax in g.axes.flat:
        ax.set_title(fullsit(ax.get_title()))
        if ax.get_xlabel() == "size":
            ax.set_xlabel("attack size")
            ax.set_xticklabels(xticks, rotation=40, ha="center")
            ax.set_xticklabels(xticks, fontsize=8)
        elif ax.get_xlabel() == "level":
            ax.set_xlabel("alliance size")

    # g.add_legend(title='Algorithm')


# In[4]:



def flatten_evidence(evdict, filterval=None):
    dumps = {}
    for source, evidence_dumps in evdict.items():
        assert len(evidence_dumps) == 2
        for evnum, evidence in enumerate(evidence_dumps):
            for k, v in evidence.items():
                tgt, msg = k.split("/")
                for trans, amount in v.items():
                    tupl = (source, tgt, msg, trans, amount)
                    dumps.setdefault(evnum, []).append(tupl)
    ret = []
    for k in sorted(dumps.keys()):
        if filterval:
            ret.append(list(filter(lambda x: x[3] == filterval, dumps[k])))
        else:
            ret.append(dumps[k])
    assert dumps[0] == ret[0]
    assert dumps[1] == ret[1]
    return tuple(ret)

def filter_evidence(evidence, **kwargs):
    fields = {'src': 0, 'tgt':1, 'type': 2, 'ev':3, 'amount': 4}
    for field, val in kwargs.items():
        if field in fields.keys():
            evidence = filter(lambda x: x[fields[field]] == val, evidence)
    return list(evidence)


def changes(set1, set2):
    removed = [x for x in set(set1).difference(set(set2))]
    added = [x for x in set(set2).difference(set(set1))]
    untouched = [x for x in set(set1).union(set(set2))]
    return (added, removed, untouched)


def condense_changes(add, rem):
    ret = {}
    for r in rem:
        ret.setdefault((r[0], r[1], r[2], r[3]), [None, None])[0] = r[4]
    for a in add:
        ret.setdefault((a[0], a[1], a[2], a[3]), [None, None])[1] = a[4]
    return ret

def cprint(changes, skip_source=True, identifier=None):
    for k, v in sorted(changes.items()):
        src, dst, msg, ev = k
        v1, v2 = v
        if identifier:
            print(identifier, sh(src), sh(dst), msg, ev, '{}->{}'.format(v1, v2))
        else:
            print(sh(src), sh(dst), msg, ev, '{}->{}'.format(v1, v2))


def tolerate(change_dict, tolerance=3):
    return {k: v for k, v in change_dict.items() if abs(v[1] - v[0]) > tolerance}



def compare_between_runs(mdf, msg_filter="deploy", tolerance=1, debug=False):
    """ compares repeated runs with shared evidence kb """
    """ compares the after state of run1 with the before state of run2"""
    """ in theory they should be the same """
    """ then compares the after state of run2 with the before state of run3"""
    """ etc """
    gr = mdf.groupby(level=0)
    comp = []
    for name, group in gr:
        timeline = {}
        rows = list(group.iterrows()) # [(index, entry), ...]
        index, values = rows[0]
        before, after = flatten_evidence(values["evidence"])
        after = filter_evidence(after, type='deploy')
        before = filter_evidence(before, type='deploy')
        add, rem, _ = changes(before, after)
        run_dict = condense_changes(add, rem)
        if debug:
            for k, v in run_dict.items():
                timeline.setdefault(k, []).append(v)
        for n, r in enumerate(rows[1:]):
            index, values = r
            before, new_after = flatten_evidence(values["evidence"])
            new_after = filter_evidence(new_after, type='deploy')
            before = filter_evidence(before, type='deploy')
            add, rem, _ = changes(after, before)
            change_dict = condense_changes(add, rem)
            add, rem, _ = changes(before, new_after)
            run_dict = condense_changes(add, rem)
            after = new_after
            if debug:
                if change_dict:
                    cprint(change_dict, identifier='{} {} run {}<->{}'.format(
                        name, values['situation'], str(n), str(n + 1)))
                    #if intermediate change
                    for k, v in change_dict.items():
                        timeline.setdefault(k, []).append(v[1] - v[0])

                for k, v in run_dict.items():
                    timeline.setdefault(k, []).append(v)
            if change_dict:
                change_dict = tolerate(change_dict, tolerance=tolerance)
                comp.append(change_dict)
        for k, v in timeline.items():
            print(k, v)

    return comp


def compare_between_datasets(mdf, mdf2, msg_filter="deploy", tolerance=0, debug=False):
    """compares evidence between two datasets"""
    rows = list(zip(mdf.iterrows(), mdf2.iterrows()))
    comp = []
    n = 0
    for r1, r2 in rows:
        index1, values1 = r1
        index2, values2 = r2

        if debug:
            debug = '{} {}'.format(values1['oid'],  values2['oid'])

        before1, after1 = flatten_evidence(values1["evidence"])
        before1 = filter_evidence(before1, type='deploy')
        after1 = filter_evidence(after1, type='deploy')
        runtime1 = int(values1["recover"]) - int(values1["detect"])

        before2, after2 = flatten_evidence(values2["evidence"])
        before2 = filter_evidence(before2, type='deploy')
        after2 = filter_evidence(after2, type='deploy')
        runtime2 = int(values2["recover"]) - int(values2["detect"])

        add, rem, same = changes(before1, before2)
        ch_before = condense_changes(add, rem)
        ch_before = tolerate(ch_before, tolerance=tolerance)
        add, rem, same = changes(after1, after2)
        ch_after = condense_changes(add, rem)
        ch_after = tolerate(ch_after, tolerance=tolerance)
        if debug:
            if abs(runtime2-runtime1) != 0:
                print('runtime(0):', runtime1, ' runtime({}):'.format(n),
                      runtime2, 'diff', runtime2 - runtime1)
            cprint(ch_before, identifier='   |- ' + debug + ' before')
            cprint(ch_after, identifier='   |- ' + debug + ' after ')
        if ch_before:
            comp.append(ch_before)
        if ch_after:
            comp.append(ch_after)
        n += 1
    return comp


def compare_ranking(mdf):
    gr = mdf.groupby(level=0)
    comp = []
    for name, group in gr:
        timeline = {}
        rows = list(group.iterrows()) # [(index, entry), ...]
        for index, values in rows:
            for count, ranking in enumerate(values["ranking"][eh('service101')]):
                runstr = ""
                r = ["node{0}({1:.1g},{2:.1g},{3})".format(sh(h)[-2:], ri, ben, rand) for h, ri, ben, rand in ranking]
                runstr = ' '.join(r)
                print(values['file'][-2:], str(index), count, runstr)
    pass

def ranking(mdf):
    gr = mdf.groupby(level=0)
    comp = []
    rankings = []
    for name, group in gr:
        timeline = {}
        rows = list(group.iterrows()) # [(index, entry), ...]
        for index, values in rows:
            if values['approach'] != 4:
                continue
            for count, ranking in enumerate(values["ranking"][eh('service101')]):
                for n, rank in enumerate(ranking):
                    h, ri, ben, rand = rank
                    h = eh(h)
                    rankings.append((index[0], index[1], values["situation"], n, h, ri, ben, rand))
    return rankings


def compare_between_experiments(mdf, sit, tolerance=0, msg_filter="deploy", debug=False):
    """compares repeated experiments with fresh evdb"""
    mdf = mdf[mdf["situation"] == sit]

    comp = []
    gr = mdf.groupby(level=1)
    for name, group in gr:
        i = name
        gcomp = []
        rows = list(group.iterrows())
        index, values = rows[0]
        firstindex = index
        before, after = flatten_evidence(values["evidence"])
        before = filter_evidence(before, type='deploy')
        after = filter_evidence(after, type='deploy')
        runtime = int(values["recover"]) - int(values["detect"])
        for index, values in rows[1:]:
            new_before, new_after = flatten_evidence(values["evidence"])
            new_before = filter_evidence(new_before, type='deploy')
            new_after = filter_evidence(new_after, type='deploy')

            add_before, rem_before, same = changes(before, new_before)
            ch_before = condense_changes(add_before, rem_before)
            ch_before = tolerate(ch_before, tolerance=tolerance)

            add_after, rem_after, same = changes(after, new_after)
            ch_after = condense_changes(add_after, rem_after)
            ch_after = tolerate(ch_after, tolerance=tolerance)
            new_runtime = int(values["recover"]) - int(values["detect"])

            if debug:
                if abs(runtime-new_runtime) != 0:
                    print('runtime({}):'.format(str(firstindex)), runtime,
                          ' runtime({}):'.format(str(index)),
                          new_runtime, 'diff', new_runtime - runtime)
                cprint(ch_before, identifier = '   |- ' + str(name) + ' before')
                cprint(ch_after, identifier =  '   |- ' + str(name) + ' after ')
            if ch_before:
                comp.append(ch_before)
            if ch_after:
                comp.append(ch_after)
    return any(comp)



def compare_single(mdf, tolerance=1, print_changes=False):
    debug = print_changes
    f = compare_between_runs(mdf, debug=debug, tolerance=tolerance)
    print("[set1] difference between runs (tolerance {}):".format(tolerance), any(f))


    f = [
        compare_between_experiments(mdf, situation, tolerance=tolerance, debug=debug)
        for situation in set(mdf["situation"])
    ]
    print("[set1] difference between iterations (tolerance {}):".format(tolerance), any(f))


def compare_all(mdf, mdf2, tolerance=1, print_changes=False):
    compare_single(mdf, tolerance=tolerance, print_changes=print_changes)
    compare_single(mdf2, tolerance=tolerance, print_changes=print_changes)

    f = compare_between_datasets(mdf, mdf2, tolerance=tolerance, debug=print_changes)
    print("[all ]difference between datasets tolerance {}:".format(tolerance), any(f))

@build_df
def line_df(scenario, situation):
    f_list = [
        "successtestlongfixed2{}{}{}".format(num, scenarios[scenario].name, situation) for num in [""]
        #"kbbasic{}{}{}".format(num, scenarios[scenario].name, situation) for num in [""]
    ]
    return f_list


@build_df
def line2_df(scenario, situation):
    f_list = [
        "newfixed{}{}{}".format(num, scenarios[scenario].name, situation) for num in [""]
    ]
    return f_list


if __name__ == '__main__':
    scenario = 0  # only look at budget datasets for now
    situations = ['1f']
    debug = True
    msg_filter = "deploy"
    mdf = build_mdf(line_df, situations=situations, scenario=scenario).set_index(["pid", "seq"]).sort_index()
    #mdf2 = build_mdf(line2_df, situations=situations).set_index(["pid", "seq"]).sort_index()
    #compare_single(mdf, tolerance=1, print_changes=debug)
    compare_ranking(mdf)
    #compare_all(mdf, mdf2, tolerance=1, print_changes=debug)
