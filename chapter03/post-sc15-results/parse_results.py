from results import *
from base import *

import json

def list_to_dict(l, key):
    res = dict()
    for item in l:
        res[item[key]] = item
    return res

def parse_iface(ifaces):
    for iface in ifaces: 
        if 'filter' not in iface.keys():
            iface['filter'] = ''
    return ifaces

def parse_settings(settings):
    res = list()
    for s in settings:
        if type(s) == type(str()): 
            res.append(json.loads(s))
        if type(s) == type(dict()):
            res.append(s)
    return parse_network(res)


def parse_network(settings):
    res = list()
    for s in settings:
        s['ifaces_dict'] = list_to_dict(parse_iface(s['ifaces']), 'name')
        res.append(s)
    return list_to_dict(res, 'name')

def parse_submissions(submissions, key='settings'):
    for s in submissions:
        s['settings_dict'] = parse_settings(s[key]) 
    return submissions

def calculate_cost(submissions):
    #cost is ((bandwidth * double_counted_links)/2) * 10) + 500 * filter
    res=list()
    for submission in submissions:
        cost = 0
        cost_nf = 0 
        if 'network_dict' not in submission.keys():
            cost=-1
            continue
        for k, v in submission['network_dict'].items():
            for k, v in v['ifaces_dict'].items():
                if v['state'] == 'up':
                    rate = int(v['rate'][:-4])
                    cost += rate
                    if v['filter']:
                        cost_nf += 500
        submission['cost'] = int((cost / 2) * 10 + cost_nf)
        res.append(submission)
    return res

for b in base.values():
    b['network_dict']=parse_network(b['network'])
    b['cost']=calculate_cost([b])[0]['cost']

def classify_submissions(submissions):
    res = list()
    for bk, bv in base.items():
        for s in submissions:
            diff = [item for item in s['settings_dict'].keys() if item not in bv['network_dict'].keys()]
            if diff == []:
                s['net_type'] = bk 
                #s['network_dict'] = {**bv['network_dict'], **s['settings_dict']}
                #s['network_dict'] = {**bv['settings_dict'], **s['network_dict']}
                s['network_dict'] = {}
                for k, v in bv['network_dict'].items():
                    if k in s['settings_dict']:
                        s['network_dict'][k] = s['settings_dict'][k]
                    else:
                        s['network_dict'][k] = v
                res.append(s)
    return res

def print_submissions(submissions):
    for s in submissions:
        print(s['net_type'], s['user'], s['revenue'])
        for k, v in s['settings_dict'].items():
            print("    " + k)
            for k, v in v['ifaces_dict'].items():
                print("        " + k)
                print("            state: " + v['state'])
                print("            rate: " + v['rate'])
                print("            filter: " + v['filter'])

def print_changes(submissions):
    for s in submissions:
        print(s['net_type'], s['user'], s['revenue'], s['cost'], s['cost_ratio'], s['rev'], s['time'])
        c = s['counters'] 
        if c['all_changes'] != (c['rate_changes'] + c['filter_changes'] + c['state_changes']):
             print("COUNT ERROR")
        for k, v in c.items():
            print ("   ",  k, ":",  v) 

def submission_changes(submission):
    changes=[]
    for nk, nv in submission['network_dict'].items():
        for ik, iv in nv['ifaces_dict'].items():
            for sk, sv in iv.items():
                oldval=base[submission['net_type']]['network_dict'][nk]['ifaces_dict'][ik][sk].strip().lower()
                newval=sv.strip().lower()
                if oldval != newval:
                    if sk == "rate":
                        oldval=int(oldval[:-4])
                        newval=int(newval[:-4])
                    change={"key": sk.strip().lower(), "node" : nk.strip().lower(), "iface" : ik.strip().lower(), "from" : oldval, "to": newval}
                    changes.append(change)
    return changes   

def collect_changes(submissions):
    res = list()
    for r in submissions:
        if 'counters' not in r.keys():
            r['counters']=dict()
        changes=submission_changes(r)
        r["changes"] = changes
        r["counters"]["all_changes"] = len(changes)
        r["counters"]["filter_udp"] = len([item for item in changes if item['key']=='filter' and item['to']== 'udp'])
        r["counters"]["filter_tcp"] = len([item for item in changes if item['key']=='filter' and item['to']== 'tcp'])
        r["counters"]["rateup"] = len([item for item in changes if item['key']=='rate' and item['from'] < item['to']])
        r["counters"]["ratedown"] = len([item for item in changes if item['key']=='rate' and item['from'] > item['to']])
        r["counters"]["stateup"] = len([item for item in changes if item['key']=='state' and item['to'] == 'up'])
        r["counters"]["statedown"] = len([item for item in changes if item['key']=='state' and item['to'] == 'down'])
        r["counters"]['state_changes'] = int(r["counters"]['stateup'] + r["counters"]['statedown'])
        r["counters"]['filter_changes'] = int(r["counters"]['filter_udp'] + r["counters"]['filter_tcp'])
        r["counters"]['rate_changes'] = int(r["counters"]['rateup'] + r["counters"]['ratedown'])
        r['cost_ratio'] = int((100/base[r['net_type']]['cost']) * r['cost']) 
        r['rev'] = int(float(r['revenue']) / base_rev[r['net_type']])
        if 'time' not in r.keys():
            r['time'] = int(float(r['verify_time']))
        res.append(r)
    return res


def export_submissions(filename, submissions, fields=[]): 
    import json
    res = list()
    for r in submissions:
        res.append({k:v for k,v in r.items() if k in fields})
    with open(filename, 'w', encoding="utf8") as outfile:
        json.dump(res, outfile, indent=1)
    return res


def read_scenario_log():
    all_submissions=[]
    with open("_scenario.log") as f:
        count = 0
        for line in f:
            j = json.loads(line)
            if 'time' not in j.keys():
                j['time'] = -1
            if 'time' in j.keys():
                j['user'] = j['session']
                all_submissions.append(j)
    return all_submissions

from operator import itemgetter

def diff_changes(sub, sub2):
    diff=[]
    dupes=[]
    for s in sub:
        for s2 in sub2: 
            if s2['user'] == s['user'] and s['user'] not in dupes:
                dupes.append(s['user'])
                if len(s['changes']) != len(s2['changes']):
                     print("NO MATCH", s['user'], len(s['changes']), len(s2['changes']))
                     ch = {item['iface']:item for item in s['changes']}
                     ch2 = {item['iface']:item for item in s2['changes']}
                     for iface, change in ch.items():
                         if iface not in ch2.keys():
                             print(change)
                     for iface, change in ch2.items():
                         if iface not in ch.keys():
                             print(change)

def parse_tasks(events):
    changes={}
    for event in events:
        if event[:len('monitoring')] == 'monitoring':
            ev=event.split(" ")
            if ev[1] == "event=LINK_TASK":
                trans={'link-filter' : 'filter', 'link-egress' : 'rate', 'link-status' : 'state'}
                iface=ev[2].split('=',1)[1]
                key=trans[ev[3][5:]]
                value=ev[4][3:-2].lower()
                changes[iface] = { 'key' : key, 'value' : value, 'name' : iface }
    return(changes)

def determine_net(submission, changes):
        iface_names = [ change['name'].lower() for change in changes.values() ]
        if len(iface_names) == 0:
            return None 
        res=-1
        for k, v in base.items():
           match=[]
           iface_names2 = []
           for n in base[k]['network']:
               if2 = [ if2['name'].lower() for if2 in n['ifaces']]
               iface_names2 += if2

           for iface in iface_names:
               if iface in iface_names2:
                    match.append(iface)

           if len(match) == len(iface_names) and not len(match) == 0:
                res = k
           else:
               #print("length:", len(match), len(iface_names))
               #from pprint import pprint
               #pprint([iface for iface in iface_names if iface not in match])
               #pprint([iface for iface in iface_names2 if iface not in match])
                pass               
        return res 
            

def apply_changes(submissions):
    res = list()
    for s in submissions:
         changes=parse_tasks(s['events'])
         net_type = determine_net(s, changes)
         if not net_type:
             s['net_type'] = -1
             continue
         s['net_type'] = net_type
         import copy
         new_dict=copy.deepcopy(base[s['net_type']]['network_dict'])
         for n in new_dict.values():
              for i in changes.keys():
                 if i in n['ifaces_dict'].keys():
                    if changes[i]['value'] == "none":
                        changes[i]['value'] = "" #returns as none but internally we use ""
                    n['ifaces_dict'][i][changes[i]['key']]=changes[i]['value']
         s['network_dict']=copy.deepcopy(new_dict)
         res.append(s)
    return res

def merge_submissions(*sub_list):
    all_submissions=list()
    done={}
    tmp = list()
    for s in sub_list:
        tmp += s
    for submission in tmp:
        if submission['user'] not in done:
            done[submission['user']] = 1
            all_submissions.append(submission)
        else:
            done[submission['user']] += 1
    return all_submissions

#logstash submissions from events
from copy import deepcopy
logstash_submit_event = [result for result in deepcopy(results) if 'SUBMIT' in result['events']]
logstash_submit_event = apply_changes(logstash_submit_event)
logstash_submit_event = calculate_cost(logstash_submit_event)
logstash_submit_event = collect_changes(logstash_submit_event)

logstash_all_data = [result for result in deepcopy(results) if 'revenue' in result.keys() and 'verify_time' in result.keys()]
logstash_all_data = apply_changes(logstash_all_data)
logstash_all_data = calculate_cost(logstash_all_data)
logstash_all_data = collect_changes(logstash_all_data)

#logstash submissions from settings 
logstash_submissions = [result for result in deepcopy(results) if 'SUBMIT' in result['events']]
logstash_submissions = parse_submissions(logstash_submissions)
logstash_submissions = classify_submissions(logstash_submissions)
logstash_submissions = calculate_cost(logstash_submissions)
logstash_submissions = collect_changes(logstash_submissions)

#make sure submissions and submissions3 is the same

#submissions from event log
logfile_submissions = parse_submissions(read_scenario_log(), key='network')
logfile_submissions = classify_submissions(logfile_submissions)
logfile_submissions = calculate_cost(logfile_submissions)
logfile_submissions = collect_changes(logfile_submissions)

#diff_changes(submissions, submissions3)

all_submissions=merge_submissions(logstash_submit_event, logstash_all_data, logstash_submissions, logfile_submissions)

def print_submission_changes(submissions, users):
    for s in submissions:
        if s['user'] in users: 
            print()
            print(s['user'])
            changes = sorted(s['changes'], key=itemgetter('node'), reverse=True)
            for i in changes:
                print(i['node'],i['key'],i['from'], i['to'])

#print_submission_changes(all_submissions, ['b8db1b0d7f8e50a095a419b7e0f04134','6f1e0e7a690aae61b6951c619a3edaa5', 'ed48788923d6c693234a935c351918f9'])


export_keys=["counters", "changes", "net_type", "rev", "revenue", "cost", "cost_ratio", "user", "time", "_0_init"]
export_submissions("processed_results.txt", all_submissions, export_keys)
print_changes(all_submissions)
print('[',','.join(["'{}'".format(s['user']) for s in all_submissions if s['net_type']==2]),']')

