import json as json
import pprint as pp
from operator import itemgetter
import csv

all_submissions=[]
no_time=[]
results=[]
fast_submit=[]


with open("processed_results.txt") as f:
        all_submissions = json.load(f, encoding='utf8')

results = [ r for r in all_submissions if r['counters']['all_changes'] > 0]
no_ops = [ r for r in all_submissions if r['counters']['all_changes'] == 0]
fast_submit=[ r for r in all_submissions if r['time'] < 4 ]
results = [r for r in results if r not in fast_submit and r['time']] #last one is a duplicate same user
results = sorted(results, key=itemgetter('rev'), reverse=True) #sort results based on revenue


def rank_data(res):
    for r in res:
        r['ranks'] = r['cost_ratio'] - r['rev']
    res = sorted(res, key=itemgetter('ranks'), reverse=False)
    for c, r in enumerate(res):
        r['rank']=c+1 


def write_csv(net_type):
    if net_type > 0:
        res=[r for r in results if r['net_type'] == net_type]
    else:
        res=[r for r in results]

    with open('data_s' + str(net_type) + '.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile,  dialect='excel')
        for r in res:
                #print(r['_0_init'], end = sep)
                #print(str(r['user']), end = sep)
                #print(str(r['time']), end = sep)
                writer.writerow([
                     r['time'],
                     r['revenue'],
#                    r['rev'],
#                    r['cost_ratio'],
                    r['cost'],
                    r['counters']['all_changes'], 
                    r['counters']['state_changes'],
                    r['counters']['rateup'],
                    r['counters']['ratedown'],
                    r['counters']['filter_changes'],
                ])
write_csv(1)
write_csv(2)
